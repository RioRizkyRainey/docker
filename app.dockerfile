FROM php:7.1-fpm

RUN apt-get update && apt-get install -y libmcrypt-dev \
    mysql-client libmagickwand-dev --no-install-recommends \
    && pecl install imagick \
    && docker-php-ext-enable imagick \
    && docker-php-ext-install mcrypt pdo_mysql

# Install the PHP mongodb
RUN /bin/bash -lc "pecl install mongodb" && \
  docker-php-ext-enable mongodb


COPY php/config/php.ini /usr/local/etc/php/
