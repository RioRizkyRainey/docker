FROM mariadb:10.3.2

# ADD mariadb/my.cnf /etc/mysql/conf.d/my.cnf

CMD ["mysqld"]

CMD /usr/scripts/scripts/mysql_install_db --defaults-file=~/.my.cnf

EXPOSE 3307
