FROM golang:1.8

COPY . .

RUN go get -d -v ./...
RUN go install -v ./...

RUN go get github.com/astaxie/beego
RUN go get github.com/beego/bee
RUN go get github.com/beego/i18n
RUN go get github.com/go-sql-driver/mysql
RUN go get github.com/astaxie/beego/httplib
RUN go get github.com/astaxie/beego/session/redis
RUN go get github.com/astaxie/beego/cache
RUN go get github.com/astaxie/beego/validation
RUN go get github.com/astaxie/beego/toolbox

RUN go build -o related.idntimes.com main.go

# EXPOSE 8080
