FROM nginx:1.10

RUN ["apt-get", "clean"]
RUN ["apt-get", "update"]
# add Certificate
COPY nginx/backend.idntimes.dev.cert /etc/nginx/ssl/certificate.cert
COPY nginx/backend.idntimes.dev.key /etc/nginx/ssl/certificate.key

ADD nginx/vhost.conf /etc/nginx/conf.d/default.conf

# RUN ["apt-get", "update"]
# RUN ["apt-get", "install", "-y", "nano"]
